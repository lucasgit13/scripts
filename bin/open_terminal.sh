#!/bin/sh
open_st() {
    st &
    exit
}

open_xterm() {
    xterm &
    exit
}
command -v st 1>/dev/null && open_st
command -v xterm 1>/dev/null && open_xterm
