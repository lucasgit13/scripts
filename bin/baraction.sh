#!/bin/bash
# Example Bar Action Script for Linux.
# Requires: acpi, iostat, lm-sensors, aptitude.
# Tested on: Arch Linux

############################## 
#	    DISK
##############################

hdd() {
	  hdd="$(df -h / | grep / | awk '{print $3 "/" $2}')"
	    echo -e " $hdd"
    }
##############################
#	    RAM
##############################

mem() {
	ram=$(free -h | awk '/Mem:/ {printf $3 "/" $2}')
echo $ram
}
##############################	
#	    CPU
##############################

cpu() {
	temp=$(sensors | awk '/^CPU Temperature:/ {print $3}')
	  read cpu a b c previdle rest < /proc/stat
	    prevtotal=$((a+b+c+previdle))
	      sleep 0.5
	        read cpu a b c idle rest < /proc/stat
		  total=$((a+b+c+idle))
		    cpu=$((100*( (total-prevtotal) - (idle-previdle) ) / (total-prevtotal) ))
		      echo -e "  $cpu% $temp"
	      }
##############################
#	    VOLUME
##############################

vol() {
	vol="$(amixer get Master | awk -F'[][]' 'END{ print $2}')"
	echo -e " $vol"
}

##############################
#	    NETWORK
##############################

network() {
wifi="$(wget -q --spider http://google.com ; echo $?)"

if [ $wifi = 0 ]; then
    echo " "
else 
    echo ""
fi
}

      SLEEP_SEC=30
      #loops forever outputting a line every SLEEP_SEC secs
      while :; do     
    echo "$(cpu) |  $(mem) |  $(hdd) |  $(vol) | $(network) |"
		sleep $SLEEP_SEC
		done
