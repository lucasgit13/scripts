#!/usr/bin/env python
#
import random
import pyperclip
import webbrowser

# Variables
clip = pyperclip.paste()    # Get the content of the clipboard
index = int(clip.rfind('/')) + 1   # Get the index value of the last '/' plus one
check = clip.find("https://", 0)

# List of Invidious instances
instances = ("https://invidious.snopyta.org/", "https://invidious.xyz/", "https://invidious.kavin.rocks/",
             "https://tube/", ".connect.cafe/", "https://invidious.zapashcanon.fr/", "https://tube.incognet.io/",
             "https://invidious.fdn.fr/", "https://invidiou.site/", "https://yewtu.be/")

link_id = (clip[index:])

# Get a random instance from 'instances' list
random_instance = random.randint(0, len(instances)-1)

# Append the Youtube video id to invidious instance
link = (instances[random_instance] + link_id)

if check == 0:
    # Open a browser with the video on a invidious instance
    webbrowser.open(link)
    print(link)
else:
    print("Not a youtube url")
