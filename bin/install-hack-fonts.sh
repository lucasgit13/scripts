#!/usr/bin/env bash
[ "$UID" -eq 1000 ] && echo "this script must be run as root" && exit 1
cmds=( wget unzip )
for cmd in "${cmds[@]}" ; do
    command -v "$cmd" >/dev/null || { echo "[$cmd] is not installed!" && pacman -S "$cmd" --noconfirm || exit 1; }
done

temp_dir="$(mktemp -dt --suffix=.SUFFIX install_hack_fonts_sh.XXXXXX)"
dest_dir=/usr/local/share/fonts/

if [ -n "$1" ]; then
    dest_dir=$(realpath "$1")
else
    [ -d "$dest_dir" ] || mkdir "$dest_dir"
fi

trap cleanup 0 1 2 15

cleanup() {
    rm -rf "temp_dir"
    echo "Cleannig up and quitting."
}

mkdir -p "$temp_dir"/output
cd "$temp_dir" || exit

fonts=( Hack Mononoki SourceCodePro Ubuntu UbuntuMono )

for font in "${fonts[@]}" ; do
    echo "Dowloading $font..." &&
    wget -q --show-progress "https://github.com/ryanoasis/nerd-fonts/releases/download/v2.3.3/$font.zip" &&
    echo "Unzipping $font..." &&
    unzip -q "$font".zip -d output/"$font" && echo "$font unziped succefully"
    echo ""
done

mv output/* "$dest_dir"
fc-cache -f -v
