#!/bin/sh
notify_f() {
    dunstify "$1" -t 1000
}
sleep 1
warp-cli status | grep -q Connected && warp-cli disconnect && notify_f 'Disconnected' && exit
warp-cli connect && notify_f 'Connected'
