#!/bin/sh

html="
<!DOCTYPE html>
<html lang=\"en\">

  <head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Drawer</title>
    <!-- <link rel=\"stylesheet\" href=\"style.css?version=1\"> -->
    <!-- <script src=\"index.js?version=1\"></script> -->
  </head>

  <body>
      <img src=\"./draw.png?version=1\">
  </body>

</html>
"

# ps aux | grep '/[tmp]/draw-server' | awk '{print $2}' && echo "$(ps aux | grep '/[tmp]/draw-server' | awk '{print $2}')"

folder='/tmp/draw-server'
image="$folder/draw.png"

[ -d "$folder" ] && rm -rf "$folder"

[ ! -d "$folder" ] && mkdir -p "$folder" && cd "$folder" && echo "$html" > index.html

scrot -s "$image"

# ps -aux | grep -i 'live-server' | grep -q node || live-server -q "$folder" &
live-server --port=0 -q "$folder" &
ps aux | grep '/[tmp]/draw-server' | awk '{print $2}'

