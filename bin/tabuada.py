#!/usr/bin/env python
from random import randint
import time
import subprocess

userInput = ''

prod = 0
tres = []
quatro = []
cinco = []
seis = []
sete = []
oito = []
nove = []

last_list_ref = []

print('Enter `q` to exit.')
while True:
    go = True
    number1 = randint(3,9)
    number2 = randint(3,9)

    if number1 == 3:
        if number2 in tres:
            go = False
        else:
            last_list_ref = tres
            tres.append(number2)

    if number1 == 4:
        if number2 in quatro:
            go = False
        else:
            last_list_ref = quatro
            quatro.append(number2)

    if number1 == 5:
        if number2 in cinco:
            go = False
        else:
            last_list_ref = cinco
            cinco.append(number2)

    if number1 == 6:
        if number2 in seis:
            go = False
        else:
            last_list_ref = seis
            seis.append(number2)

    if number1 == 7:
        if number2 in sete:
            go = False
        else:
            last_list_ref = sete
            sete.append(number2)

    if number1 == 8:
        if number2 in oito:
            go = False
        else:
            last_list_ref = oito
            oito.append(number2)

    if number1 == 9:
        if number2 in nove:
            go = False
        else:
            last_list_ref = nove
            nove.append(number2)


    if go:
        prod = number1*number2

        userInput = input(f'{number1} X {number2}: ')

        if userInput == 'q':
            break

        if prod == int(userInput):
            print('\tCorreto!')
            time.sleep(0.5)
            # print(f'tres = {tres}, quatro = {quatro}')
        else:
            print(f"\tErrado! {prod}")
            last_list_ref.pop()
            time.sleep(0.5)

    if len(tres) + len(quatro) + len(cinco) + len(seis) + len(sete) + len(oito) + len(nove) == 49: break

    subprocess.run(['clear'])
    print('Enter `q` to exit.')




