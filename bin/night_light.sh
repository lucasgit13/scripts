#!/bin/sh
value=5700
n=150
while [ "$value" -gt 3500 ]; do
    redshift -P -O $value >/dev/null
    value=$((value-n))
    sleep 0.2
done
