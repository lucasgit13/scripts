#!/bin/sh
move_x=$1
move_y=$2
click=$3
[ "$click" = 1 ] && xdotool mousedown 1 && sleep 1 && xdotool mouseup 1 && exit 0
[ "$click" = 3 ] && xdotool mousedown 3 && sleep 1 && xdotool mouseup 3 && exit 0
x=$(xdotool getmouselocation --shell | awk -F '=' '/^X/ {print $2}')
y=$(xdotool getmouselocation --shell | awk -F '=' '/^Y/ {print $2}')

xdotool mousemove $((x + move_x)) $((y + move_y))
echo $((x + move_x)) $((y + move_y))

