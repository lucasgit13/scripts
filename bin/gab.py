#!/usr/bin/env python
import json

questions = {'1': ''}

count = int(input('Start from question: '))

while True:
    ans = input(f'Q{count}: ')

    if ans == 'W':
        ans1 = int(input('1. Write to a file\n2. Print\n3. Back\n:'))

        if ans1 == 1:
            with open("gabarito.txt", "w") as file:
                file.write(json.dumps(questions))
                file.close()

        elif ans1 == 2:
            print(json.dumps(questions, indent=4))

    if ans:
        string = str(count)
        questions[string] = ans
        count+=1

        # print(questions)
