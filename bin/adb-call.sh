#!/bin/sh
. "$HOME"/.scripts/settings/config

call() {
    adb shell am start -a android.intent.action.CALL -d tel:"${1}" ; exit 0
}

[ -n "$1" ] && call "$1"

choice=$(fzf < "$DIR"/contacts.txt | cut -d= -f2)

[ -n "$choice" ] && call "$choice"
