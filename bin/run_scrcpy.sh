#!/bin/sh
serial=$(adb devices -l | grep -w 'device' | cut -d ' ' -f1 | head -n1)
xdotool search --class scrcpy && xdotool key --clearmodifiers super+9 ||
scrcpy -S --shortcut-mod=lctrl -s "$serial"
