#!/bin/sh
sessions="$HOME/.config/sessions.txt"
if [ "$1" ]; then
    if grep -qE "\[[$1]+\]" "$sessions" ; then
        # cp -f "$HOME/.xinitrc" "$HOME/.xinitrc.bak"
        sed -n "/$1/,/^$/{//!p}" "$sessions" | sed '/^#/d' > "$HOME/.xinitrc"
else
    printf '%s' "No option for: $1"
    fi
else
    printf "Options:\n\t1. dwm\n\t2. openbox"
fi
