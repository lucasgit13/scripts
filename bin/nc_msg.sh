#!/bin/sh
addr="$1"
port="$2"
test -n "$addr" || exit 1
test -n "$port" || exit 1
msg="foo"
while true
do
    read msg
    echo "$msg"
done | nc "$addr" "$port"
